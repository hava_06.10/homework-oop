#include <iostream>
using namespace std;
int main()
{
    setlocale(LC_ALL, "RUSSIAN");
    int i, n, m, z, t, f;
    cout << "Введите начало интервала: ";
    cin >> n;
    cout << "Введите конец интервала: ";
    cin >> m;
    for (i = n; i <= m; i++)
    {
        z = i;
        f = 1;
        while (z)
        {
            t = z % 10;
            if (t == 0 || i % t != 0) { f = 0; break; }
            z /= 10;
        }
        if (f == 1) cout << i << endl;
    }
    return 0;
}
