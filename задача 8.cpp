#include <iostream>
#include <cmath>
 
int main()
{
    setlocale(LC_ALL, "Russian");
    
    int n = 0;
    int m = 0;
    
    std::cout << "Введите два числа: ";    
    std::cin >> n >> m;
    
    if (n > m)
    {
        int tmp = m;
        m = n;
        n = tmp;
    }
    
    std::cout << "Пифагоровы тройки чисел от " 
              << n << " до " << m << " включительно:" << std::endl;
    for (int i = n; i <= m; i++)
    {
        for (int j = n; j <= m; j++)
        {
            for (int k = n; k <= m; k++)
            {
                if (i*i+j*j==k*k)  
                    std::cout << "{ " << i << ";" << j << ";" << k << "}" <<std::endl; 
            }
        }
    }
}
