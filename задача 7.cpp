#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "RUSSIAN");
    int x, k, n;
    cout << "Вводите числа от 1 до 250" << endl;
    cin >> n;
    while (n < 1 || n > 250) std::cin >> n;
    for (int i = 0; i <= n; i++)
    {
        cin >> x;
        k = 0;
        for (int j = 1; j <= x; j++)
        {
            if (x % j == 0)
            {
                k++;
            }
        }
        cout << x << " = " << k << endl;
    }

    return 0;
}
