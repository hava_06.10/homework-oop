#include <iostream>
#include <math.h>

using namespace std;

void main()
{
    int n;
    cin >> n;
    int sum = 0;
    for (int i = 5; i < n; i++)
    {
        sum = 0;

        for (int j = 1; j < (int)(sqrt(i * 1.0)) + 1; j++)
            if (i % j == 0)
            {
                sum += j;
                if (j != i / 2 && j > 1) sum += i / j;
            }

        if (i == sum) cout << i << " ";
    }
    system("pause");
}
