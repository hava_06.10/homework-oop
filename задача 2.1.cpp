#include <iostream>
using namespace std;
int main() {
  int a, b;

  cout << "Enter a: ";
  
  if (!(cin >> a)) {
    cout << "Error! " << endl;   
    cin.clear();   
    return -1;
  }

  cout << "Enter b: ";

  if (!(cin >> b)) {
    cout << "Error! " << endl;  
    cin.clear();    
    return -1;
  }

  cout << " a " << " + " << " b " << " = " << a + b << endl;
  return 0;
}
