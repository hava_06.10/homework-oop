#include <iostream>
using namespace std;

int main()
{
    setlocale(LC_ALL, "RUSSIAN");
    int n, res = 0, res2 = 0;
    cout << "Введите число: ";
    cin >> n;
    int i = 1;
    do {
        if (i % 2 == 0)
            res += i;
        if (i % 2 != 0)
            res2 += i; i++;
    } while (i < n);

    cout << " сумма четных: " << res << "\n сумма нечетных: " << res2 << "\n";
    system("pause");
    return 0;
}
