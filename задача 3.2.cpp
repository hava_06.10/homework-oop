#include <iostream>
#include <cmath>
using namespace std;
int main() {
  int x;

  while (true){
    cout << "Enter number from 0 to 100: ";
     if (!(cin >> x)) {
      cout << "Error! " << endl;   
    }

    if (x < 0 || x > 100){
      cout << "Error! Enter number from 0 to 100! \n";
    }
    else {
      break;
    }
  }
  cout << x << "^5 = " << x * x * x * x * x << endl;
  return 0;
}
